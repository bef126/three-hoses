package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"appengine"

	"github.com/mzimmerman/appenginetesting"
)

func MockContext(t *testing.T) (*appenginetesting.Context, error) {
	c, err := appenginetesting.NewContext(&appenginetesting.Options{
		AppId:   "three-hoses",
		Testing: t,
		Debug:   appenginetesting.LogChild,
		Modules: []appenginetesting.ModuleConfig{
			{
				Name: "default",
				Path: filepath.Join("app.yaml"),
			},
		},
	})

	return c, err
}

func MockPost(path string, args url.Values, t *testing.T, c *appenginetesting.Context) (string, int, error) {
	defHost, err2 := appengine.ModuleHostname(c, "default", "", "")
	if err2 != nil {
		return "", 0, err2
	}

	resp, err3 := http.PostForm("http://"+defHost+path, args)
	if err3 != nil {
		return "", 0, err3
	}

	bodyBytes, err4 := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err4 != nil {
		return "", 0, err4
	}

	body := strings.TrimSpace(string(bodyBytes))

	return body, resp.StatusCode, nil
}

// parse JSON string into obj
func ParseJsonIntoStruct(j string, obj interface{}) error {
	dec := json.NewDecoder(strings.NewReader(j))
	err := dec.Decode(obj)
	return err
}

// create a single test function so we can share the same context
func TestApiWrapper(t *testing.T) {
	c, err := MockContext(t)
	if err != nil {
		t.Error(err)
	}
	defer c.Close()

	MyTestLookupApiHandler(t, c)
	MyTestAllApiHandler(t, c)
}

func MyTestLookupApiHandler(t *testing.T, c *appenginetesting.Context) {
	values := make(url.Values)
	values.Set("lat", "30.359385")
	values.Set("lng", "-97.738109")

	path := "/v1/lookup"
	_, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 200 {
		t.Errorf("statusCode was %d - expecting 200", statusCode)
	}
}

func MyTestAllApiHandler(t *testing.T, c *appenginetesting.Context) {
	_, _ = createStation("A", "foo", 30.1, -97.0, true, c)
	// datastore limited to 1 write per second
	duration := time.Second
	time.Sleep(duration)
	_, _ = createStation("B", "foo", 30.1, -97.0, true, c)
	// datastore limited to 1 write per second
	time.Sleep(duration)

	values := make(url.Values)
	values.Set("foo", "bar")
	path := "/v1/all"
	body, statusCode, err := MockPost(path, values, t, c)
	t.Error(body)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 200 {
		t.Errorf("statusCode was %d - expecting 200", statusCode)
	}
}
