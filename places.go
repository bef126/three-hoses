package main

import (
	"appengine"
	"appengine/urlfetch"
	"encoding/json"
	"fmt"
)

type Loc struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type Geo struct {
	Location Loc `json:"location"`
}

type Place struct {
	Name           string `json:"name"`
	GooglePlacesId string `json:"place_id"`
	Geometry       Geo    `json:"geometry"`
}

type JsonResponse struct {
	Results []Place `json:"results"`
}

func appendIfMissing(slice []Place, i Place) []Place {
	for _, ele := range slice {
		if ele.GooglePlacesId == i.GooglePlacesId {
			return slice
		}
	}
	return append(slice, i)
}

func nearbyPlaces(lat, lng float64, c appengine.Context) ([]Place, error) {
	var ret []Place

	searchRadiusInMeters := "100"
	url := fmt.Sprintf("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%v,%v&radius=%v&types=gas_station&key=%v", lat, lng, searchRadiusInMeters, GooglePlacesApiKey)
	client := urlfetch.Client(c)
	resp, err := client.Get(url)
	if err != nil {
		return ret, err
	}

	dec := json.NewDecoder(resp.Body)
	var r JsonResponse
	err3 := dec.Decode(&r)
	if err3 != nil {
		return ret, err3
	}

	for _, v := range r.Results {
		ret = appendIfMissing(ret, v)
	}
	return ret, nil
}
