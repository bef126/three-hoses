package main

import (
	"appengine/aetest"
	"testing"
)

func TestNearbyPlaces(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	_, err = nearbyPlaces(30.359385, -97.738109, c)
	if err != nil {
		t.Fatal(err)
	}
}
