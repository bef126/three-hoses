package main

import (
	"appengine"
	"appengine/aetest"
	"appengine/datastore"
	"testing"
	"time"
)

func TestStationJSON(t *testing.T) {
	s := Station{
		Name:                   "Joe's Gas Station",
		Location:               appengine.GeoPoint{Lat: 30.0, Lng: -97.0},
		OneHoseConfirmations:   2,
		ThreeHoseConfirmations: 3,
		GooglePlacesId:         "foo",
	}
	result := s.JSON()
	expected := `{"Name":"Joe's Gas Station","Location":{"Lat":30,"Lng":-97},"OneHoseConfirmations":2,"ThreeHoseConfirmations":3,"GooglePlacesId":"foo","LastUpdatedAt":"0001-01-01T00:00:00Z","DistanceInMiles":0,"StringId":""}`
	if result != expected {
		t.Errorf("result=%v expected=%v", result, expected)
	}
}

// GeoPoints must have lats between -90/90 and lngs between -180/180
func TestCreateStationWithInvalidLocation(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const name = "Joe's Gas Station"

	_, err = createStation(name, "foo", 91, 181, true, c)
	if err == nil {
		t.Error("Expected Error")
	}
	if err.Error() != "Invalid lat/lng" {
		t.Errorf("result=[%v] expected=[Invalid lat/lng]", err.Error())
	}
}

func TestCreateStation(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const name = "Joe's Gas Station"

	s, err := createStation(name, "foo", 30, -97, true, c)
	if err != nil {
		t.Error(err.Error())
	}
	if s.Name != name {
		t.Errorf("result=%v expected=%v", s.Name, name)
	}
	if s.StringId == "" {
		t.Error("StringId should not be nil")
	}
	if s.OneHoseConfirmations != 0 && s.ThreeHoseConfirmations != 1 {
		t.Error("Confirmation counts are wrong")
	}
}

func TestNearybyStations(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	s1, err1 := createStation("A", "foo", 30.3, -97.0, true, c)
	if err1 != nil {
		t.Error(err1.Error())
	}
	// datastore limited to 1 write per second
	duration := time.Second
	time.Sleep(duration)

	s2, err2 := createStation("B", "bar", 30.1, -97.0, true, c)
	if err2 != nil {
		t.Error(err2.Error())
	}
	time.Sleep(duration)

	s3, err3 := createStation("C", "baz", 30.2, -97.0, true, c)
	if err3 != nil {
		t.Error(err3.Error())
	}
	time.Sleep(duration)

	stations, err4 := nearbyStations(30.0, -97.0, -1, c)
	if err4 != nil {
		t.Error(err4.Error())
	}

	if len(stations) != 3 {
		t.Fatalf("result=%v expected=%v", len(stations), 3)
	}

	if stations[0].Name != s2.Name {
		t.Errorf("result=%v expected=%v", stations[0].Name, s2.Name)
	}

	if stations[1].Name != s3.Name {
		t.Errorf("result=%v expected=%v", stations[1].Name, s3.Name)
	}

	if stations[2].Name != s1.Name {
		t.Errorf("result=%v expected=%v", stations[2].Name, s1.Name)
	}
}

func TestNearybyStationsWithLimit(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	_, err1 := createStation("A", "foo", 30.1, -97.0, true, c)
	if err1 != nil {
		t.Error(err1.Error())
	}
	// datastore limited to 1 write per second
	duration := time.Second
	time.Sleep(duration)

	_, err2 := createStation("B", "bar", 30.2, -97.0, true, c)
	if err2 != nil {
		t.Error(err2.Error())
	}
	time.Sleep(duration)

	stations, err3 := nearbyStations(30.0, -97.0, 1, c)
	if err3 != nil {
		t.Error(err3.Error())
	}

	if len(stations) != 1 {
		t.Fatalf("result=%v expected=%v", len(stations), 1)
	}
}

func TestStationExists(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	// create a station
	s := &Station{
		GooglePlacesId: "abc123",
	}

	parentKey := datastore.NewKey(c, "Station", "default_station", 0, nil)
	key := datastore.NewIncompleteKey(c, "Station", parentKey)

	key, err2 := datastore.Put(c, key, s)
	if err2 != nil {
		t.Fatal(err2)
	}

	// datastore limited to 1 write per second
	duration := time.Second
	time.Sleep(duration)

	s.StringId = key.Encode()

	// test the negative
	x, b, _ := stationExists("foo", c)
	if b == true {
		t.Error("foo should not exist")
	}

	// test the positive
	x, b, _ = stationExists("abc123", c)
	if b == false {
		t.Error("abc123 should exist")
	}

	if x.StringId != s.StringId {
		t.Error(x.StringId)
		t.Error(s.StringId)
		t.Error("keys do not match")
	}
}

func TestRetrieveAllStations(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	_, _ = createStation("A", "foo", 30.1, -97.0, true, c)
	_, _ = createStation("B", "foo", 30.1, -97.0, true, c)

	results, err2 := retrieveAllStations(c)
	if err2 != nil {
		t.Fatal(err2)
	}

	if len(results) != 2 {
		t.Fatal("length should be 2")
	}

}
