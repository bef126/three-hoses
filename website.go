package main

import (
	"appengine"
	"appengine/taskqueue"
	"fmt"
	gccontext "google.golang.org/appengine"
	"net/http"
	"net/url"
)

func init() {
	http.HandleFunc("/", aboutHandler)
	http.HandleFunc("/map", mapHandler)
	http.HandleFunc("/discuss", discussHandler)
	http.HandleFunc("/data", dataHandler)
	http.HandleFunc("/gen", generateFileHandler)
	http.HandleFunc("/genworker", generateFileWorker)
}

func aboutHandler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"Foo": "Bar",
	}
	renderTemplate(w, "about.html", params)
}

func mapHandler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"Foo": "Bar",
	}
	renderTemplate(w, "map.html", params)
}

func discussHandler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"Foo": "Bar",
	}
	renderTemplate(w, "discussion.html", params)
}

func dataHandler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"Foo": "Bar",
	}
	renderTemplate(w, "data.html", params)
}

func generateFileHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	t := taskqueue.NewPOSTTask("/genworker", url.Values{"foo": {"bar"}})

	// Add the task to the default queue.
	_, err := taskqueue.Add(c, t, "")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "OK")
}

func generateFileWorker(w http.ResponseWriter, r *http.Request) {
	c := gccontext.NewContext(r)
	c2 := appengine.NewContext(r)
	err := saveFile(c, c2)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "OK")
}
