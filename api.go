package main

import (
	"appengine"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
)

// An input parameter for each API method
type Parameter struct {
	Name     string
	Required bool
}

func init() {
	http.HandleFunc("/v1/create", createApiHandler)
	http.HandleFunc("/v1/search", searchApiHandler)
	http.HandleFunc("/v1/lookup", lookupApiHandler)
	http.HandleFunc("/v1/all", allApiHandler)
}

func validateParameters(params []Parameter, r *http.Request) error {
	for _, v := range params {
		if v.Required {
			if r.FormValue(v.Name) == "" {
				return errors.New(fmt.Sprintf("%v is required\n", v.Name))
			}
		}
	}
	return nil
}

func createApiHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	params := []Parameter{
		Parameter{Name: "lat", Required: true},
		Parameter{Name: "lng", Required: true},
		Parameter{Name: "id", Required: true},
		Parameter{Name: "hoses", Required: true},
	}
	paramsErr := validateParameters(params, r)
	if paramsErr != nil {
		http.Error(w, paramsErr.Error(), http.StatusInternalServerError)
		return
	}

	lat, _ := strconv.ParseFloat(r.FormValue("lat"), 64)
	lng, _ := strconv.ParseFloat(r.FormValue("lng"), 64)
	name := r.FormValue("name")
	id := r.FormValue("id")
	hoses, _ := strconv.ParseInt(r.FormValue("hoses"), 10, 0)

	threeHoses := hoses > 1

	s, err := createStation(name, id, lat, lng, threeHoses, c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, s.JSON())
}

func searchApiHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	params := []Parameter{
		Parameter{Name: "lat", Required: true},
		Parameter{Name: "lng", Required: true},
	}
	paramsErr := validateParameters(params, r)
	if paramsErr != nil {
		http.Error(w, paramsErr.Error(), http.StatusInternalServerError)
		return
	}

	lat, _ := strconv.ParseFloat(r.FormValue("lat"), 64)
	lng, _ := strconv.ParseFloat(r.FormValue("lng"), 64)

	results, err := nearbyStations(lat, lng, 5, c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	b, err2 := json.Marshal(results)
	if err != nil {
		http.Error(w, err2.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(b[:]))
}

func lookupApiHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	params := []Parameter{
		Parameter{Name: "lat", Required: true},
		Parameter{Name: "lng", Required: true},
	}
	paramsErr := validateParameters(params, r)
	if paramsErr != nil {
		http.Error(w, paramsErr.Error(), http.StatusInternalServerError)
		return
	}

	lat, _ := strconv.ParseFloat(r.FormValue("lat"), 64)
	lng, _ := strconv.ParseFloat(r.FormValue("lng"), 64)

	results, err := nearbyPlaces(lat, lng, c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	b, err2 := json.Marshal(results)
	if err != nil {
		http.Error(w, err2.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(b[:]))
}

func allApiHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	results, err := retrieveAllStations(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	b, err2 := json.Marshal(results)
	if err != nil {
		http.Error(w, err2.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(b[:]))
}
