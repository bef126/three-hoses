package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"time"

	"net/http"
)

func readTemplate(filename string) (string, error) {
	ba, err := ioutil.ReadFile("templates/" + filename)
	if err != nil {
		return "", err
	}

	// convert the byte array to a string
	raw := string(ba)

	return raw, nil
}

func renderTemplate(w http.ResponseWriter, t string, p map[string]string) {
	structure, err := readTemplate("template.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	content, err := readTemplate(t)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// instantiate the outer-template
	tmpl, err := template.New("name").Parse(structure)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var ac, mc, dc, xc, title string = "active", "", "", "", ""
	if t == "map.html" {
		mc = "active"
		ac = ""
		title = "Map"
	} else if t == "discussion.html" {
		dc = "active"
		ac = ""
		title = "Discuss"
	} else if t == "data.html" {
		xc = "active"
		ac = ""
		title = "Data Dump"
	} else {
		title = "About"
	}

	// fill out the outer template
	params := map[string]interface{}{
		"Content":         template.HTML(content),
		"AboutClass":      ac,
		"MapClass":        mc,
		"DiscussionClass": dc,
		"DataClass":       xc,
		"Title":           title,
	}
	var doc bytes.Buffer
	err = tmpl.Execute(&doc, params)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// instantiate the inner-template
	tmpl, err = template.New("name").Parse(doc.String())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// execute the inner-template against parameters p
	err = tmpl.Execute(w, p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func TimeAgoInWords(t time.Time) string {
	secondsDiff := time.Now().Sub(t).Seconds()
	daysDiff := secondsDiff / 86400.0
	if daysDiff < 1.0 {
		if secondsDiff < 10.0 {
			return "just now"
		}
		if secondsDiff < 60.0 {
			return fmt.Sprintf("%v seconds ago", secondsDiff)
		}
		if secondsDiff < 120.0 {
			return "a minute ago"
		}
		if secondsDiff < 3600.0 {
			return fmt.Sprintf("%v minutes ago", int(secondsDiff/60.0))
		}
		if secondsDiff < 7200.0 {
			return "an hour ago"
		}
		if secondsDiff < 86400.0 {
			return fmt.Sprintf("%v hours ago", int(secondsDiff/3600.0))
		}
	}
	if daysDiff < 2.0 {
		return "Yesterday"
	}
	if daysDiff < 7.0 {
		return fmt.Sprintf("%v days ago", int(daysDiff))
	}
	if daysDiff < 31.0 {
		return fmt.Sprintf("%v weeks ago", int(daysDiff/7.0))
	}
	if daysDiff < 365.0 {
		return fmt.Sprintf("%v months ago", int(daysDiff/30.0))
	}
	return fmt.Sprintf("%v years ago", int(daysDiff/365.0))
}
