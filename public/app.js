var ThreeHoses = {};

ThreeHoses.map = null;
ThreeHoses.count = 0;
ThreeHoses.visibleInfoWindow = null;

ThreeHoses.bootstrap = function(mmap){
  ThreeHoses.map = mmap;
  ThreeHoses.drawPins();
}

ThreeHoses.formatInfoWindow = function(station){
  var content = "";
  content += "<h5>" + station.Name + "</h5>";
  content += "<table border='0'>";
  content += "<tr><td align='left'><small>One Hose Check-ins:</small></td><td align='right'><small>" + station.OneHoseConfirmations + "</small></td></tr>";
  content += "<tr><td align='left'><small>Three Hose Check-ins:</small></td><td align='right'><small>" + station.ThreeHoseConfirmations + "</small></td></tr>";
  content += "<tr><td align='left'><small>Last Check-in:</small></td><td align='right'><small>" + station.FormatedTime + "</small></td></tr>";
  content += "</table>";
  return content;
}

ThreeHoses.drawPin = function(ll, station){
  var image;
  if(station.ThreeHoseConfirmations > 0){
     image = {
      url: 'public/3-icon-big.png',
      size: new google.maps.Size(30, 46),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(15, 46)
    };
  } else {
    image = {
      url: 'public/1-icon-big.png',
      size: new google.maps.Size(30, 46),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(15, 46)
    };
  }
  var marker = new google.maps.Marker({
    position: ll,
    map: ThreeHoses.map,
    icon: image
  });

  var infowindow = new google.maps.InfoWindow({content: ThreeHoses.formatInfoWindow(station)});
  google.maps.event.addListener(marker, 'click', function () {
    if(ThreeHoses.visibleInfoWindow){
      ThreeHoses.visibleInfoWindow.close();
    }
    infowindow.open(ThreeHoses.map, this);
    ThreeHoses.visibleInfoWindow = infowindow;
  });
}

ThreeHoses.drawPins = function(){
  var request = $.ajax({
    url: "/v1/all",
    method: "GET",
    dataType: "json"
  });
   
  request.done(function( data ) {
    for (var i=0; i<data.length; i++){
      station = data[i];
      ll = new google.maps.LatLng(station.Location.Lat, station.Location.Lng);
      ThreeHoses.drawPin(ll, station);
    }
  });
   
  request.fail(function( jqXHR, textStatus ) {
    console.log( "Request failed: " + textStatus );
  });
}
