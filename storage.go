package main

import (
	ae "appengine"
	"bytes"
	"errors"
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/appengine"
	"google.golang.org/appengine/file"
	"google.golang.org/appengine/urlfetch"
	"google.golang.org/cloud"
	"google.golang.org/cloud/storage"
)

// bucket is a local cache of the app's default bucket name.
var bucket = "three-hoses.appspot.com"

// name of file to upload
const fileName = "three-hoses.csv"

func generateFile(c ae.Context) ([]byte, error) {
	var buffer bytes.Buffer

	stations, err := retrieveAllStations(c)
	if err != nil {
		return buffer.Bytes(), err
	}

	// write out the headers
	buffer.WriteString("googlePlacesId,lat,lng,name,oneHoseConfirmations,threeHoseConfirmations,lastUpdatedAt")

	// write the contents
	for _, s := range stations {
		buffer.WriteString(fmt.Sprintf(`%v,%v,%v,"%v",%v,%v,%v`, s.GooglePlacesId, s.Location.Lat, s.Location.Lng, s.Name, s.OneHoseConfirmations, s.ThreeHoseConfirmations, s.LastUpdatedAt))
	}

	return buffer.Bytes(), nil
}

func saveFile(c context.Context, c2 ae.Context) error {
	data, err1 := generateFile(c2)
	if err1 != nil {
		return err1
	}

	if bucket == "" {
		var err error
		if bucket, err = file.DefaultBucketName(c); err != nil {
			return errors.New("failed to get default bucket")
		}
	}
	hc := &http.Client{
		Transport: &oauth2.Transport{
			Source: google.AppEngineTokenSource(c, storage.ScopeFullControl),
			Base:   &urlfetch.Transport{Context: c},
		},
	}
	ctx := cloud.NewContext(appengine.AppID(c), hc)

	// attempt to delete the old file before we write the new one
	storage.DeleteObject(ctx, bucket, fileName)

	// write the new file
	wc := storage.NewWriter(ctx, bucket, fileName)
	wc.ContentType = "text/csv"

	if _, err := wc.Write(data); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}

	// make it world-readable
	err := storage.PutACLRule(ctx, bucket, fileName, "allUsers", storage.RoleReader)
	if err != nil {
		return err
	}

	return nil
}
