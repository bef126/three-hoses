package main

import (
	"appengine"
	"appengine/datastore"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"sort"
	"time"
)

type Station struct {
	Name                   string
	Location               appengine.GeoPoint
	OneHoseConfirmations   int
	ThreeHoseConfirmations int
	GooglePlacesId         string
	LastUpdatedAt          time.Time
	DistanceInMiles        float64 `datastore:"-"`
	StringId               string  `datastore:"-"`
	FormatedTime           string  `datastore:"-"`
}

func (s Station) JSON() string {
	b, err := json.Marshal(s)
	if err != nil {
		fmt.Println("error:", err)
		return ""
	}
	return string(b[:])
}

func createGeoPoint(lat, lng float64) (appengine.GeoPoint, error) {
	g := appengine.GeoPoint{Lat: lat, Lng: lng}

	if g.Valid() == false {
		return appengine.GeoPoint{}, errors.New("Invalid lat/lng")
	}

	return g, nil
}

func stationExists(id string, c appengine.Context) (Station, bool, error) {
	q := datastore.NewQuery("Station").Filter("GooglePlacesId =", id).Limit(1)
	var results []Station
	keys, err := q.GetAll(c, &results)
	if err != nil {
		return Station{}, false, err
	}
	for i, p := range results {
		key := keys[i]
		p.StringId = key.Encode()
		return p, true, nil
	}
	return Station{}, false, nil
}

func incOneHoseCounter(skey string, c appengine.Context) error {
	key, kerr := datastore.DecodeKey(skey)
	if kerr != nil {
		return kerr
	}

	s := new(Station)
	err := datastore.RunInTransaction(c, func(c appengine.Context) error {
		err := datastore.Get(c, key, s)
		if err != nil && err != datastore.ErrNoSuchEntity {
			return err
		}
		s.OneHoseConfirmations++
		s.LastUpdatedAt = time.Now()
		_, err = datastore.Put(c, key, s)
		return err
	}, nil)
	if err != nil {
		return err
	}
	return nil
}

func incThreeHoseCounter(skey string, c appengine.Context) error {
	key, kerr := datastore.DecodeKey(skey)
	if kerr != nil {
		return kerr
	}

	s := new(Station)
	err := datastore.RunInTransaction(c, func(c appengine.Context) error {
		err := datastore.Get(c, key, s)
		if err != nil && err != datastore.ErrNoSuchEntity {
			return err
		}
		s.ThreeHoseConfirmations++
		s.LastUpdatedAt = time.Now()
		_, err = datastore.Put(c, key, s)
		return err
	}, nil)
	if err != nil {
		return err
	}
	return nil
}

func createStation(name, id string, lat, lng float64, threeHoses bool, c appengine.Context) (Station, error) {
	g, gerr := createGeoPoint(lat, lng)
	if gerr != nil {
		return Station{}, gerr
	}

	st, b, e := stationExists(id, c)
	if e != nil {
		return Station{}, e
	}
	if b == true {
		if threeHoses == true {
			ie := incThreeHoseCounter(st.StringId, c)
			if ie != nil {
				return st, ie
			}
		} else {
			ie := incOneHoseCounter(st.StringId, c)
			if ie != nil {
				return st, ie
			}
		}
		return st, nil
	}

	s := &Station{
		Name:                   name,
		Location:               g,
		GooglePlacesId:         id,
		OneHoseConfirmations:   0,
		ThreeHoseConfirmations: 0,
		LastUpdatedAt:          time.Now(),
	}

	// the following creates an eventual consistency key
	//key := datastore.NewIncompleteKey(c, "Station", nil)

	// the following creates an immediately consistent key
	parentKey := datastore.NewKey(c, "Station", "default_station", 0, nil)
	key := datastore.NewIncompleteKey(c, "Station", parentKey)

	key, err := datastore.Put(c, key, s)
	if err != nil {
		return Station{}, err
	}

	s.StringId = key.Encode()

	if threeHoses == true {
		ie := incThreeHoseCounter(s.StringId, c)
		if ie != nil {
			return st, ie
		}
	} else {
		ie := incOneHoseCounter(s.StringId, c)
		if ie != nil {
			return st, ie
		}
	}
	return *s, nil
}

// ByDistance implements sort.Interface for []Station based on
// the DistanceInMiles field.
type ByDistance []Station

func (a ByDistance) Len() int           { return len(a) }
func (a ByDistance) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDistance) Less(i, j int) bool { return a[i].DistanceInMiles < a[j].DistanceInMiles }

// limit of -1 implies all records
func nearbyStations(lat, lng float64, limit int, c appengine.Context) ([]Station, error) {
	var ret []Station
	var results []Station

	const maxSearchRadiusInMiles = 25.0

	_, gerr := createGeoPoint(lat, lng)
	if gerr != nil {
		return ret, gerr
	}

	q := datastore.NewQuery("Station").Filter("ThreeHoseConfirmations >", 0)

	var stations []Station
	keys, err := q.GetAll(c, &stations)
	if err != nil {
		return ret, err
	}

	// since google's datastore doesn't have native geo-queries, we have to do
	// everything in the Go layer. As long as the dataset doesn't grow too large
	// this shouldn't be a major problem.
	// For now, we pull back all the results, calculate the distance for each one,
	// then sort by distance
	for i, s := range stations {
		key := keys[i]
		s.StringId = key.Encode()
		s.DistanceInMiles = haversine(lat, lng, s.Location.Lat, s.Location.Lng)
		if s.DistanceInMiles <= maxSearchRadiusInMiles {
			results = append(results, s)
		}
	}
	sort.Sort(ByDistance(results))
	if limit > 0 {
		if len(results) > limit {
			return results[0:limit], nil
		} else {
			return results, nil
		}
	} else {
		return results, nil
	}
}

func toRadians(num float64) float64 {
	return num * math.Pi / 180
}

func haversine(lat1, lng1, lat2, lng2 float64) float64 {
	r := 3959.0 // radius of earth in miles
	dLat := toRadians(lat2 - lat1)
	dLng := toRadians(lng2 - lng1)
	latone := toRadians(lat1)
	lattwo := toRadians(lat2)

	a := math.Sin(dLat/2.0)*math.Sin(dLat/2.0) + math.Sin(dLng/2.0)*math.Sin(dLng/2.0)*math.Cos(latone)*math.Cos(lattwo)
	c := 2.0 * math.Asin(math.Sqrt(a))

	return math.Abs(r * c)
}

func retrieveAllStations(c appengine.Context) ([]Station, error) {
	var results []Station
	var ret []Station

	q := datastore.NewQuery("Station")
	keys, err := q.GetAll(c, &results)
	if err != nil {
		return results, err
	}
	for i, s := range results {
		key := keys[i]
		s.StringId = key.Encode()
		s.FormatedTime = TimeAgoInWords(s.LastUpdatedAt)
		ret = append(ret, s)
	}

	return ret, nil
}
