package main

import (
	"net/http"
)

func init() {
	http.HandleFunc("/admin/home", adminHomeHandler)
}

func adminHomeHandler(w http.ResponseWriter, r *http.Request) {
	params := map[string]string{
		"Foo": "Bar",
	}
	renderTemplate(w, "admin.html", params)
}
